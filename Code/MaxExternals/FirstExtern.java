import com.cycling74.max.*;

public class FirstExtern extends MaxObject
   {

   private static final String[] INLET_ASSIST = new String[]{
           "Input any data type"
   };

   private static final String[] OUTLET_ASSIST = new String[]{
           "Output data"
   };


   public FirstExtern(Atom[] args)
      {
      declareInlets(new int[]{DataTypes.ALL});
      declareOutlets(new int[]{DataTypes.ALL});

      setInletAssist(INLET_ASSIST);
      setOutletAssist(OUTLET_ASSIST);


      }

   public void bang()
      {
      post("First External, written in Intellij");


      }

   public void inlet(int i)
      {
      }


   public void inlet(float f)
      {
      }


   public void list(Atom[] list)
      {
      }

   }






