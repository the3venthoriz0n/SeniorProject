import com.cycling74.max.*;

/**
 * Created by MrPC on 11/10/15.
 */
public class Counter2 extends MaxObject implements Executable
   {

   private MaxClock clock;
   private int count = 0;
   private float interval = 400.f;

   public Counter2()
      {
      declareAttribute("count");
      declareAttribute("interval");
      // this refers to Counter2. MaxClock takes an executable as arg
      clock = new MaxClock(this);
      }

   public void inlet(int i)
      {
      if (i == 1) // if toggle is set to true for inlet 1
         clock.delay(0); //run execute method
      else
         notifyDeleted();
      }

   public void execute()// this method will be called when it is time for MaxClock to perform
      {
      bang();
      clock.delay(interval);
      }

   protected void notifyDeleted()
      {
      clock.unset();
      }

   public void bang()
      {
      count++;
      outlet(0, count);
      }
   }
