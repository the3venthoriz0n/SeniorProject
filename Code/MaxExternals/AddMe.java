import com.cycling74.max.*;

import javax.xml.crypto.Data;

/**
 * Created by MrPC on 11/10/15.
 */
public class AddMe extends MaxObject
   {

   private Atom addend = Atom.newAtom(0);

   public AddMe(Atom[] args)
      {
      declareIO(2, 1); // set number of inputs, outputs  of datatype.all
      if (args.length > 0)
         {
         if (args[0].isInt() || args[0].isFloat())
            {
            addend = args[0];
            }
         }
      }

   public void inlet(int i)
      {
      if (getInlet() == 0)
         {
         if (addend.isInt())
            {
            outlet(0, i + addend.getInt());
            }
         else
            {
            outlet(0, (float) i + addend.getFloat());
            }
         }
      else
         {
         if (addend.isInt())
            {
            addend = Atom.newAtom(i);
            }
         else
            {
            addend = Atom.newAtom((float) i);
            }
         }
      }

   public void inlet(float f)
      {
      if (getInlet() == 0)
         {
         if (addend.isInt())
            {
            outlet(0, (int) f + addend.getInt());
            }
         else
            {
            outlet(0, f + addend.getFloat());
            }
         }
      else
         {
         if (addend.isInt())
            {
            addend = Atom.newAtom((int) f);
            }
         else
            {
            addend = Atom.newAtom(f);
            }
         }
      }
   }

