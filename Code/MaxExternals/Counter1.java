import com.cycling74.max.*;

/**
 * Created by MrPC on 11/10/15.
 */
public class Counter1 extends MaxObject
   {

   private int count = 0;

   public Counter1()
      {
      declareAttribute("count");
      }

   public void bang()
      {
      count++;
      outlet(0, count);
      }
   }

