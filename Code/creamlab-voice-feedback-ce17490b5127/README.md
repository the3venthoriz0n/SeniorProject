# README #

DAVID
https://bitbucket.org/creamlab/voice-feedback

version 1.0
15/10/2015

D.A.V.I.D. (Da amazing voice inflection device) is a real-time voice transformation tool able to “colour” any voice recording with an emotion that wasn’t intended by it’s speaker. D.A.V.I.D. was especially designed with affective psychology and neuroscientist in mind, and aims to provide researchers with new ways to produce and control affective stimuli, both for offline listening and for real-time paradigms.

### LICENSE ###

The MIT License (MIT)

Copyright (c) 2015 CNRS UMR 9912 STMS / IRCAM

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

### HOWTO ###

To open the patch, double click on the DAVID.maxproj file

# INPUT SOUND

DAVID can be either used with sound files, by choosing and playing them from the upper left part of the patch ; either with a microphone (first input channel of the audio card), by hitting on the [MICROPHONE OFF] button.

# EFFECTS

4 effects modules are available in the central panel, with their own parameters to set: constant pitch shift, vibrato, inflexion, filter.

# CONSOLE AND AUTOMATION

The lower panel provides 4 effect sliders and a [MASTER] slider. Each effect slider is related to the corresponding processing module, and control its intensity: 100% gives the full intensity specified by the module’s parameters, while at 0% the module is excluded.

The [MASTER] slider allows to control all of the sliders activated by the current effect at once, preserving proportions; its range varies from 0% to 200%.

If the [AUTOMATION] check box is on, playing or recording a transformation makes the [MASTER] slider to automatically start a linear ramp from 0% to 100% in [RAMP TIME] seconds, after an initial time of [HOLD TIME] seconds where the effects are not applied

# PRESETS

4 default presets for the effects parameters are proposed: neutral, happy, afraid, sad. 

* recall a preset: use the panel with small squares, each square is a preset; pass upon a lighted square with the mouse to know the preset’s name, click on a square to recall a preset

* define a preset: 
- define a sliders and effects configuration you want to store
- type the preset name in the text box
- the software automatically proposes a preset number that can be used (current limit is 44 presets) 
- hit the [SAVE AS PRESET <n>] button

* overwrite a preset (presets from 1 to 4 cannot be modified): 
- recall a preset
- define a new sliders and effects configuration you want to store
- optionally type a new preset name in the text box
- hit the [OVERWRITE PRESET <n>] button

* having a list of available effects and parameters that are controlled : hit the “[SHOW PRESETS]” button

# RECORDING

To record a transformation:

* using a sound file: 
- choose the sound file with the [Open soundfile] button
- hit the [RECORD] button and choose a destination folder: recording automatically ends at the end of the file, if you don’t stop it before
- the recorded file is stored in the chosen folder, filename is given by <original filename>_<preset name>.wav

* using a sounds folder: 
- choose the sound folder with the [Open folder] button
- hit the [RECORD] button and choose a destination folder: recording automatically ends when all of the sound file in the folders have been processed
- the recorded files are stored in the chosen folder, filename is given by <original filename>_<effect name>.wav

* using a microphone
- switch on the [MIC OFF] button
- hit the [RECORD] button and choose a destination folder to start recording, hit again to end
- the recorded file is stored in the chosen folder, filename is given by <date>_<hour>.wav

Turning on the microphone excludes sound files to be recorded

### Who do I talk to? ###

* marco.liuni@ircam.fr
* cream.ircam.fr